package com.afs.tdd;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final List<Direction> directList = Arrays.asList(
            Direction.North,
            Direction.East,
            Direction.South,
            Direction.West);
}
