package com.afs.tdd.strategy;

import com.afs.tdd.Constants;
import com.afs.tdd.Location;

public class LeftCommandStrategy implements CommandStrategy {
    @Override
    public void action(Location location) {
        int locationIndex = Constants.directList.indexOf(location.getDirection());
        int currentLocationIndex = (locationIndex + Constants.directList.size() - 1) % Constants.directList.size();
        location.setDirection(Constants.directList.get(currentLocationIndex));
    }
}
