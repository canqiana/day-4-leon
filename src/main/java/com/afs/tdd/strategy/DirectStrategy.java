package com.afs.tdd.strategy;

import com.afs.tdd.Location;

public interface DirectStrategy {
    void action(Location location);
}
