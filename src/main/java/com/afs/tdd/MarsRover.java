package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        command.getCommandStrategy().action(this.location);
    }

    public void executeBatchCommands(List<Command> commandList) {
        commandList.forEach(this::executeCommand);
    }

    public Location getLocation() {
        return location;
    }
}
