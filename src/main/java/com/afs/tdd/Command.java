package com.afs.tdd;

import com.afs.tdd.strategy.CommandStrategy;
import com.afs.tdd.strategy.LeftCommandStrategy;
import com.afs.tdd.strategy.MoveCommandStrategy;
import com.afs.tdd.strategy.RightCommandStrategy;

public enum Command {
    Left(new LeftCommandStrategy()),
    Right(new RightCommandStrategy()),
    Move(new MoveCommandStrategy());

    private final CommandStrategy commandStrategy;

    Command(CommandStrategy commandStrategy) {
        this.commandStrategy = commandStrategy;
    }

    public CommandStrategy getCommandStrategy() {
        return commandStrategy;
    }
}
